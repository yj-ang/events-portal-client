const getPosts = () => import('~/static/posts.json').then(json => json.default || json)

export const state = () => ({
  posts: []
})

export const actions = {
  fetchPosts ({ commit }) {
    return new Promise(async (resolve) => {
      const res = await getPosts()

      res.data.map((post) => {
        if (post.preview.length > 230) {
          post.preview = post.preview.slice(0, 230) + '...'
        }
      })

      commit('updatePosts', res.data)

      // setTimeout(() => {
      resolve()
      // }, 2000)
    })
  }
}

export const mutations = {
  updatePosts (state, data) {
    state.posts = [ ...state.posts, ...data ]
  }
}
